
// export = public en TypeScript
export class Paciente {
  idPacientePk: number;
  nombres: string;
  apellidos: string;
  dni: string;
  direccion: string;
  telefono: string;
  email: string;
}

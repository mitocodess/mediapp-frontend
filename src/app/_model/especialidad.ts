
// export = public en TypeScript
export class Especialidad {
  idEspecialidadPk: number;
  nombre: string;
  descripcion: string;
}


// export = public en TypeScript
export class Medico {
  idMedicoPk: number;
  nombres: string;
  apellidos: string;
  fotoUrl: string;
  cmp: string;
}

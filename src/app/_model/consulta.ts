import { Especialidad } from "./especialidad";
import { Medico } from "./medico";
import { Paciente } from "./paciente";
import { DetalleConsulta } from "./detalleConsulta";

export class Consulta {

  idConsultaPk: number;
  numConsultorio: string;
  //fechaTiempo: Date;
  fechaTiempo: string; //2020-02-13T11:30:05 ISODate || moment.js

  idPacienteFk: Paciente;
  idMedicoFk: Medico;
  idEspecialidadFk: Especialidad;

  lstDetalleConsulta: DetalleConsulta[];
}



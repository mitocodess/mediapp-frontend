import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Paciente } from '../_model/paciente';
import { Subject } from 'rxjs';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class PacienteService extends GenericService<Paciente>{

  private pacienteCambio = new Subject<Paciente[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) {

    // enviando parametros al constructor padre
    super(
      http,
      `${environment.HOST}/pacientes`);
  }

  listarPageable(page:number, size:number){
    return this.http.get<any>(`${this.url}/pageable?page=${page}&&size=${size}`);
  }

  /** get, set */
  getPacienteCambio() {
    return this.pacienteCambio.asObservable();
  }
  setPacienteCambio(lista: Paciente[]) {
    this.pacienteCambio.next(lista);
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }
  setMensajeCambio(msg: string) {
    this.mensajeCambio.next(msg);
  }
}

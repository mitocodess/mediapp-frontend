import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Medico } from '../_model/medico';

import { environment } from 'src/environments/environment';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class MedicoService extends GenericService<Medico> {

  private medicoCambio = new Subject<Medico[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) {

    // enviando parametros al constructor padre
    super(
      http,
      `${environment.HOST}/medicos`);
  }

  /** get, set */
  getMedicoCambio() {
    return this.medicoCambio.asObservable();
  }
  setMedicoCambio(lista: Medico[]) {
    this.medicoCambio.next(lista);
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }
  setMensajeCambio(msg: string) {
    this.mensajeCambio.next(msg);
  }
}

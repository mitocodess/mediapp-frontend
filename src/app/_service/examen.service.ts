import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Examen } from '../_model/examen';
import { GenericService } from './generic.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExamenService extends GenericService<Examen>{

  private examenCambio = new Subject<Examen[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) {

    // enviando parametros al constructor padre
    super(
      http,
      `${environment.HOST}/examenes`);
  }

  /** get, set */
  getExamenCambio() {
    return this.examenCambio.asObservable();
  }
  setExamenCambio(lista: Examen[]) {
    this.examenCambio.next(lista);
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }
  setMensajeCambio(msg: string) {
    this.mensajeCambio.next(msg);
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { switchMap } from 'rxjs/operators';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {

  // columnas a mostrar en la tabla
  displayedColumns = ["idPaciente", "nombres", "apellidos", "acciones"];
  dataSource: MatTableDataSource<Paciente>; // es como el value de un dataTable de primefaces


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  cantidad: number = 0;

  //lista: Paciente[];

  constructor(
    private pacienteService: PacienteService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {

    this.pacienteService.getPacienteCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.pacienteService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    //this.pacienteService.listar().subscribe(data => this.lista = data);
    // ahora esta es la nueva forma de pasarle data al datasource
    // this.pacienteService.listar().subscribe(data => {
    //   this.crearTabla(data);
    // });
    // por defecto mostrar la 1ra apgina y los 10 1ro elemntos
    this.pacienteService.listarPageable(0, 10).subscribe(data => {
      //this.crearTabla(data.content);
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }

  crearTabla(data: Paciente[]) {
    this.dataSource = new MatTableDataSource(data);
    // vinculando la tabla con el paginador y el ordenador
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }


  eliminar(id: number) {
    this.pacienteService.eliminar(id).pipe(switchMap(() => {
      return this.pacienteService.listar();
    })).subscribe(data => {
      this.pacienteService.setPacienteCambio(data);
      this.pacienteService.setMensajeCambio('Se Elimino');
    });
  }

  filtrar(valor:string){
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  mostrarMas(e: any){
    this.pacienteService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }

}
